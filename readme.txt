Miles Marshall : 03 Dec 2020


The Game of Life
----------------


Rules
----------------
1.  Any live cell with fewer than two live neighbours dies
    X < 2 = 0

2.  Any live cell with two or three live neighbours lives
    X >= 2 = X

3.  Any live cell with more than three live neighbours dies
    X > 3 = 0

4.  Any dead cell with exactly three live neighbours becomes a live cell
    0 == 3 = X


Where:
  state = live/dead or X/0


Tests (X=state, 0=neighbours)
----------------
1.  0,0 = 0
2.  0,1 = 0
3.  0,2 = 0
4.  0,3 = X
5.  0,4 = 0
6.  X,0 = 0
7.  X,1 = 0
8.  X,2 = 0
9.  X,3 = X
10. X,4 = X


Output
----------------
miless-MBP-2:ozow milesmarshall$ jest index
 PASS  ./index.test.js
  ✓ 0,0 (3 ms)
  ✓ 0,1
  ✓ 0,2 (1 ms)
  ✓ 0,3
  ✓ 0,4
  ✓ 1,0
  ✓ 1,1 (1 ms)
  ✓ 1,2
  ✓ 1,3
  ✓ 1,4

Test Suites: 1 passed, 1 total
Tests:       10 passed, 10 total
Snapshots:   0 total
Time:        3.247 s
Ran all test suites matching /index/i.